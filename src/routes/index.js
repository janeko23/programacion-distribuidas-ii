const {Router} = require("express");
const router = new Router();
var xmlparser = require('express-xml-bodyparser'), xml2js = require('xml2js');
const _ = require("underscore");
var validator = require('xsd-schema-validator');
const validate = require('uuid-validate');
const {idValid} = require('../functions/functions');
/**= 
 * get all items
 */
router.get("/", (request, response) => {
    return response.status(200).json("all items sent");
});

/**
 * Get a specific item
 */
router.get("/:id", (request, response) => {
    if (request.params.id === "b172a2ab-5900-4532-bd68-68a041752017" && validate(request.params.id)) {
        return response.status(200).json("Item b172a2ab-5900-4532-bd68-68a041752017 Found");
    }
    return response.status(404).json("Item not found");
});

/**
 * Add item
 */
router.post('/', xmlparser({
    trim: false,
    explicitArray: true
}),
function (request, response) {
    var builder = new xml2js.Builder();
    var xml = builder.buildObject(request.body);
    validator.validateXML(xml, "src/resources/doc2daEntrega.xsd", function(error, result) {
        if (result.valid) {
            response.status(201).write('<div>XML Validation was correct. ITEM CREATED</div>');
        } else {
            response.status(406).write('<div>');
            response.write('<div>XML validation failed.</div>');
            response.write('<div>Error: ' + error + '</div>');
            response.write('</div>');
        }
        response.end();
    });
});

router.put('/:id', (request, response) => {
    var builder = new xml2js.Builder();
    var xml = builder.buildObject(request.body);
    if(idValid(request.params.id)){
        validator.validateXML(xml, "src/resources/doc2daEntrega.xsd", function(error, result) {
            if (result.valid) {
                response.status(201).write('<div>XML Validation was correct. ITEM UPDATE</div>');
            } else {
                response.status(406).write('<div>');
                response.write('<div>XML validation failed.</div>');
                response.write('<div>Error: ' + error + '</div>');
                response.write('</div>');
            }
            response.end();
        });
    }else{
        response.status(404).json('Error don`t exist ID')
    }
});

router.delete('/:id', (request, response) => {
    return idValid(request.params.id) ? 
    response.status(200).json(`Método DELETE en el recurso items/${request.params.id}`)
    : response.status(404).json(`Item not found`)
});


module.exports = router;