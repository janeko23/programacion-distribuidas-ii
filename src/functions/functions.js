const validate = require('uuid-validate');

function idValid(id){
    let idQueExiste = "b172a2ab-5900-4532-bd68-68a041752017"
    return id === idQueExiste && validate(id)
}
function isValidFormatPut(request) {
    let itemValidValues = Object.values(request.body)[0]
    return (idValid(request.params.id) && isValidFormatPost(request.body)) ?
        request.params.id === itemValidValues.object_id[0] : false
}
function isValidFormatPost(item){
    const array = ['RENTED', 'RETURN', 'DELIVERY TO RENT', 'DELIVERY TO RETURN'];
    const correctFormat = {
        object_id: [],
        client_id: [],
        details: [{
            status: [],
            until: []
        }]
    }
    if (JSON.stringify(item) != '{}') {
        const even = (element) => element === Object.keys(item)[0];
        let validLabel= ['rent', 'return', 'delivery_to_rent', 'delivery_to_return'].some(even)
        itemValidValues = Object.values(item)[0],
        validExistencia = itemValidValues.details && validLabel && itemValidValues.object_id && itemValidValues.client_id
        
        if (validExistencia) { 
            let validParametros = isCorrectParams(itemValidValues, correctFormat) && isCorrectParams(itemValidValues.details[0], correctFormat.details[0])
            if (validParametros) { 
                let fecha = itemValidValues.details[0].until[0], status= itemValidValues.details[0].status[0]
                const even = (element) => element === status;
                let  validStatus = array.some(even), 
                validValues = validate(itemValidValues.client_id[0]) && validate(itemValidValues.object_id[0]) && isValidDate(fecha)
                return validParametros && validValues && validStatus
            }
        }       
    }
    return false
}
// Funcion para ver si los parametros recibidos corresponen a lo esperado
function isCorrectParams(vectorA, vectorB) {
    var aPropiedades = Object.getOwnPropertyNames(vectorA).sort();
    var bPropiedades = Object.getOwnPropertyNames(vectorB).sort();
    for (var iterator = 0; iterator < bPropiedades.length; iterator++) {
        if (aPropiedades[iterator] != bPropiedades[iterator]) {
            return false;
        }
    }
    return true;
}
//funcion para validar la fecha
function isValidDate(dateString) {

    var datePattern = /^(19[5-9][0-9]|20[0-4][0-9]|2050)[/](0?[1-9]|1[0-2])[/](0?[1-9]|[12][0-9]|3[01])$/;

    var matchArray = dateString.match(datePattern);

    if (matchArray == null) {
        return false;
    }
    var cleanDateString = dateString.replace(/\D/g, '');

    var year = parseInt(cleanDateString.substr(0, 4));
    var month = parseInt(cleanDateString.substr(4, 2));
    var day = parseInt(cleanDateString.substr(6, 2));


    var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
        daysInMonth[1] = 29;
    }

    return (month < 1 || month > 12 || day < 1 || day > daysInMonth[month - 1]) ?
    false : true;
}
module.exports = {
    isValidDate,
    isCorrectParams,
    isValidFormatPost,
    isValidFormatPut, 
    idValid
}