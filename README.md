# TP VIDEOCLUB- PROGRAMACION DISTRIBIDAS 2

_En este repositorio se presenta el funcionamiento de una api rest, los requerimientos principales a tratar son los siguientes:_


### EN LA PRIMER ENTREGA:
* El sistema debe recibir información en XML.
* Se tiene que validar que  el client_id y el object_id tengan un formato de UUID (ver qué se puede validar)
* El status puede ser RENT / DELIVERY_TO_RENT / RETURN / DELIVERY_TO_RETURN.
* La fecha tiene que ser con formato AAAA/MM/DD.


### SEGUNDA ENTREGA:

_Validar el XML con un XML Schema en base a las reglas ya expuestas en la primera consigna. O sea:_

* crear un XML acorde. Establecer criterios para donde falte definición y dejarlo asentado en los comentarios
* usar dicho XML Schema para validar en el request
* La solución debe basarse en containers
_


## Comenzando 🚀
Me encuentro trabajando en Windows y las herramientas que utilizare serán las siguientes:
[Node js](https://nodejs.org/es/) - es un entorno de ejecución para JavaScript construido con el motor de JavaScript V8 de Chrome. 

_Teniendo docker podra probar el proyecto_
Crear imagen
```
    docker build -t node-restapi .
```
_Si desean ejecutarlo de manera interactiva, es decir ver por consola como se va ejecutando_

``` 
    docker run -it -p 3000:4000 node-restapi
```
_run por backgrooound_
``` 
    docker run -d -p 3000:4000 node-restapi
``` 
Mira **Deployment** para conocer como desplegar el proyecto.


## Pre-requisitos 📋

Simplemente descargue [Windows Installer](https://nodejs.org/en/#home-downloadhead) directamente desde el sitio web de [nodejs.org](https://nodejs.org/es).


## Instalación 🔧

Para instalar las dependecias npm por consola:
```
    npm install --save 
# example
    npm install --save express
```
Si se quiere hacer de forma global añadir -g:
```
    npm install -g
```
Instalar última versión o versión concreta de un paquete estos son los comandos para la última versión de un paquete:
```
    npm install @latest
```
y para instalar una versión concreta:
```
    npm install @
# example
    npm install express@4.17.1
```
Listar paquetes instalados_
La instrucción para listar los paquetes disponibles:
```
npm list
```
Eliminar dependencias npm para desinstalar dependencia y eliminar de package.json:
```
    npm rm --save 
    npm rm --save colors # example
```
_si se desea eliminar del desarrollo_
```
    npm rm --save-dev 
    npm rm --save-dev colors # example
```
para eliminar el directorio de dependencias, puede venir bien para reinstalar todo (> npm install)
```
    -rm
# example:
    rm -rd /node_modules
    npm install
```
Actualizar dependencias npm (npm-check-updates)
Podemos utilizar una dependencia llamada npm-check-updates, se encargará de checkear nuestros paquetes y comprobar sus versiones vs su última versión. Para instalarlo:
```
    npm install -g npm-check-updates
```
A partir de ese momento dispondremos de un comando global del mismo nombre (npm-check-updates), o mejor áun, abreviado como ncu, que nos permitirá hacer todo lo indicado.

```
    ncu
```
Al ejecutarlo nos mostrará las versiones actuales de nuestros paquetes y, separadas con una flecha, las más recientes.

Si queremos que se actualicen todos entonces usaremos:

```
    ncu -u
```


## Ejecutando las pruebas ⚙️

_Las pruebas automatizadas para este sistema se ejecutaran de la siguiente manera_
```
  npm test
```
![Test imagen 1](img/test1.png)
![Test imagen 2](img/test2.png)
![Test imagen 3](img/test3.png)

Con coverage
```
    npm run test-with-coverage    
```
![Test Coverage](img/testConv.png)
![Test Coverage1](img/coverage.png)


### Analice las pruebas end-to-end 🔩

Con el uso de postman o algun aplicativo que nos permite crear peticiones sobre APIs.
Podra generar las siguientes peticiones
*  Obtener todos los items 
Get
```
localhost:4000/items/
```
![GET ALL](img/getAll.png)

* Crear un item
```
localhost:4000/items/
```
BODY:
```
<rent>
   <object_id>b172a2ab-5900-4532-bd68-68a041877656</object_id>
   <client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id>
   <details>
       <status>RENT</status>
       <until>2020/10/01</until>
   </details>
</rent>
```
![POST](img/post.png)

* Actualizar
```
localhost:4000/items/b172a2ab-5900-4532-bd68-68a041752017
```

BODY:
```
<rent>
   <object_id>b172a2ab-5900-4532-bd68-68a041877656</object_id>
   <client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id>
   <details>
       <status>RENT</status>
       <until>2020/10/01</until>
   </details>
</rent>
```
![PUT](img/put.png)

* Eliminar
```
localhost:4000/items/b172a2ab-5900-4532-bd68-68a041752017
```
![DELETE](img/delete.png)


## Despliegue 📦

* Run modo normal
```
    npm start
```
![START](img/start.png)

* Run modo dev
```
    npm run dev
```
![DEV](img/dev.png)

_Teniendo docker podra probar el proyecto_
Crear imagen
```
    docker build -t node-restapi .
```
_Si desean ejecutarlo de manera interactiva, es decir ver por consola como se va ejecutando_
``` 
    docker run -it  node-restapi
```
## Construido con 🛠️

_Las herramientas que utilizaste para crear este proyecto son:_
* [NODE JS](https://nodejs.org/es) - Es un entorno de tiempo de ejecución de JavaScript
* [EXPRESS](https://expressjs.com/es/) - El framework web usado
* [MORGAN](https://www.npmjs.com/package/morgan) - Middleware for node.js
* [NODEMON](https://www.npmjs.com/package/nodemon) - El modulo web usado
* [CHAI](https://www.chaijs.com/) - Es a BDD / TDD biblioteca de aserciones para node 
* [MOCHA](https://mochajs.org/) - Es a feature-rich JavaScript test framework running on Node.js and in the browser
* [SUPERTEST](https://www.npmjs.com/package/supertest) - Módulo es proporcionar una abstracción de alto nivel para probar HTTP, mientras que todavía le permite bajar a la API de nivel inferior proporcionada por superagente.
