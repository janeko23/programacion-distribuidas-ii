var request = require('supertest'),
    express = require('express'),
    xmlparser = require('express-xml-bodyparser'),
    xml2js = require('xml2js');

const app = require("../src/app")
describe('type = "xml" should work', function () {
    it('should support xml', function (done) {
        var app = express();

        app.get('/', xmlparser({
            trim: false,
            explicitArray: true
        }), function (req, res) {
            res.set('Content-Type', 'application/xml');
            res.send('<root>Hello</root>');
        });

        var s = app.listen(function () {
            var url = 'http://localhost:' + s.address().port;
            request(url)
                .get('/')
                .set('Content-Type', 'application/xml')
                .set('Accept', 'application/xml')
                .expect('Content-Type', /xml/)
                .expect(200)
                .expect("<root>Hello</root>", done);
        });
    });

    it('.post for xml should work', function (done) {
        var app = express();

        app.post('/', xmlparser({
            trim: false,
            explicitArray: true
        }), function (req, res) {
            console.log(req.body);
            res.set('Content-Type', 'application/xml');
            var builder = new xml2js.Builder();
            var xml = builder.buildObject(req.body);
            res.send(xml);
        });

        var s = app.listen(function () {
            var url = 'http://localhost:' + s.address().port;
            request(url)
                .post('/')
                .set('Content-Type', 'application/xml')
                .set('Accept', 'application/xml')
                .send('<?xml version="1.0" encoding="utf-8" ?><rent> <object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENTED </status> <until> 2020/10/01 </until> </details> </rent>')
                .expect('Content-Type', /xml/)
                .expect(200)
                .expect('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
                    '<rent>\n' +
                    '  <object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id>\n' +
                    '  <client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id>\n' +
                    '  <details>\n' +
                    '    <status>RENTED</status>\n' +
                    '    <until>2020/10/01</until>\n' +
                    '  </details>\n' +
                    '</rent>', done);
        });
    });

});