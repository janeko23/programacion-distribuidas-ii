var request = require('supertest'),
    express = require('express'),
    xmlparser = require('express-xml-bodyparser'),
    xml2js = require('xml2js');
    var assert = require('assert');
    var expect = require('chai').expect;
    var should = require('chai').should();
const {
    isCorrectParams,
    isValidDate, isValidFormatPut, isValidFormatPost
} = require('../src/functions/functions')

const app = require("../src/app")

/**
 * Testing get all items endpoint
 */
describe("GET /items", () => {
    it("respond with json containing a list of all items", (done) => {
        request(app)
            .get("/items")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });
});

/**
 * Testing items endpoint by giving an existing item
 */
describe("GET /items/:id", () => {
    it("respond with json containing a single items", (done) => {
        request(app)
            .get("/items/b172a2ab-5900-4532-bd68-68a041752017")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("respond with json item not found when the item does not exists", (done) => {
        request(app)
            .get("/items/nonexistingitem")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(404)
            .expect('"Item not found"')
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});
/**
 * Testing POST items endpoint
 */
describe("POST /items", () => {

    it("respond with 201 created", (done) => {
        request(app)
            .post("/items")
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="utf-8" ?> <rent> <object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENT </status> <until> 2020/10/01 </until> </details> </rent>')
            .expect(201)
            .expect('<div>XML Validation was correct. ITEM CREATED</div>', done)
    });
    it("respond with 406 on bad request (le  quite el object_id)", (done) => {
        request(app)
            .post('/items')
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><rent> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENTED </status> <until> 2020/10/01 </until> </details> </rent>')
            .expect(406)
            .expect('<div><div>XML validation failed.</div><div>Error: Error: invalid xml (status=WITH_ERRORS)\n' +
  "\t[error] cvc-complex-type.2.4.a: Se ha encontrado contenido no válido a partir del elemento 'client_id'. Se esperaba uno de '{object_id}'. (3:14)\n" +
  "\t[error] cvc-enumeration-valid: El valor 'RENTED' no es de faceta válida con respecto a la enumeración '[RENT, RETURN, DELIVERY_TO_RENT, DELIVERY_TO_RETURN]'. Debe ser un valor de la enumeración. (5:28)\n" +
  "\t[error] cvc-type.3.1.3: El valor 'RENTED' del elemento 'status' no es válido. (5:28)</div></div>")
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
    it("respond with 406 on bad request. Estado invalido ", (done) => {
        request(app)
            .post('/items')
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><rent><object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> LALALA </status> <until> 2020/10/01 </until> </details> </rent>')
            .expect(406)
            .expect('<div><div>XML validation failed.</div><div>Error: Error: invalid xml (status=WITH_ERRORS)\n' +
  "\t[error] cvc-enumeration-valid: El valor 'LALALA' no es de faceta válida con respecto a la enumeración '[RENT, RETURN, DELIVERY_TO_RENT, DELIVERY_TO_RETURN]'. Debe ser un valor de la enumeración. (6:28)\n" +
  "\t[error] cvc-type.3.1.3: El valor 'LALALA' del elemento 'status' no es válido. (6:28)</div></div>")
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
    it("Le saco el estado, es decir formato invalido", (done) => {
        request(app)
            .post('/items')
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="utf-8" ?> <object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENTED </status> <until> 2020/10/01 </until> </details>')
            .expect(406)
            .expect('<div><div>XML validation failed.</div><div>Error: Error: invalid xml (status=WITH_ERRORS)\n' +
  "\t[error] cvc-elt.1: No se ha encontrado la declaración del elemento 'object_id'. (2:12)</div></div>")
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});

/**
 * Testing PUT items endpoint
 */
describe("PUT /items/:id", () => {

    it("respond with 201 update", (done) => {
        request(app)
            .put("/items/b172a2ab-5900-4532-bd68-68a041752017")
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="utf-8" ?> <rent> <object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENT </status> <until> 2020/10/01 </until> </details> </rent>')
            .expect(201)
            .expect('<div>XML Validation was correct. ITEM UPDATE</div>', done)
    });
    it("respond with 406 on bad request (le  quite el object_id)", (done) => {
        request(app)
            .put("/items/b172a2ab-5900-4532-bd68-68a041752017")
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><rent> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENT </status> <until> 2020/10/01 </until> </details> </rent>')
            .expect(406)
            .expect('<div><div>XML validation failed.</div><div>Error: Error: invalid xml (status=WITH_ERRORS)\n' +
  "\t[error] cvc-complex-type.2.4.a: Se ha encontrado contenido no válido a partir del elemento 'client_id'. Se esperaba uno de '{object_id}'. (3:14)</div></div>")
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
    it("respond with 404. ID dont Exit", (done) => {
        request(app)
            .put("/items/b172a2ab-5900-4532-bd68-68a041752018")
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="utf-8" ?> <rent> <object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENTED </status> <until> 2020/10/01 </until> </details> </rent>')
            .expect(404)
            .expect('"Error don`t exist ID"')
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
    it("Le saco la etiqueta rent, es decir formato invalido", (done) => {
        request(app)
            .put("/items/b172a2ab-5900-4532-bd68-68a041752017")
            .set('Content-Type', 'application/xml')
            .set('Accept', 'application/xml')
            .send('<?xml version="1.0" encoding="utf-8" ?> <object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENTED </status> <until> 2020/10/01 </until> </details>')
            .expect(406)
            .expect('<div><div>XML validation failed.</div><div>Error: Error: invalid xml (status=WITH_ERRORS)\n' +
  "\t[error] cvc-elt.1: No se ha encontrado la declaración del elemento 'object_id'. (2:12)</div></div>")
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});

/**
* Testing DELETE items endpoint
*/
describe("DELETE /items/:id", () => {
    it("respond with delete item", (done) => {
        request(app)
            .delete("/items/b172a2ab-5900-4532-bd68-68a041752017")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .expect('"Método DELETE en el recurso items/b172a2ab-5900-4532-bd68-68a041752017"', done)
    });

    it("respond with json item not found when the item does not exists", (done) => {
        request(app)
            .delete("/items/nonexistingitem")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(404)
            .expect('"Item not found"')
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});
describe('type = "xml" should work', function () {
    it('should support xml', function (done) {
        var app = express();

        app.get('/', xmlparser({
            trim: false,
            explicitArray: true
        }), function (req, res) {
            res.set('Content-Type', 'application/xml');
            res.send('<root>Hello</root>');
        });

        var s = app.listen(function () {
            var url = 'http://localhost:' + s.address().port;
            request(url)
                .get('/')
                .set('Content-Type', 'application/xml')
                .set('Accept', 'application/xml')
                .expect('Content-Type', /xml/)
                .expect(200)
                .expect("<root>Hello</root>", done);
        });
    });

    it('.post for xml should work', function (done) {
        var app = express();

        app.post('/', xmlparser({
            trim: false,
            explicitArray: true
        }), function (req, res) {
            console.log(req.body);
            res.set('Content-Type', 'application/xml');
            var builder = new xml2js.Builder();
            var xml = builder.buildObject(req.body);
            res.send(xml);
        });

        var s = app.listen(function () {
            var url = 'http://localhost:' + s.address().port;
            request(url)
                .post('/')
                .set('Content-Type', 'application/xml')
                .set('Accept', 'application/xml')
                .send('<?xml version="1.0" encoding="utf-8" ?><rent> <object_id> b172a2ab-5900-4532-bd68-68a041752017 </object_id> <client_id> 5d65ac9e-d431-4138-a8e4-c4719205cb1b </client_id> <details> <status> RENTED </status> <until> 2020/10/01 </until> </details> </rent>')
                .expect('Content-Type', /xml/)
                .expect(200)
                .expect('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n' +
                    '<rent>\n' +
                    '  <object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id>\n' +
                    '  <client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id>\n' +
                    '  <details>\n' +
                    '    <status>RENTED</status>\n' +
                    '    <until>2020/10/01</until>\n' +
                    '  </details>\n' +
                    '</rent>', done);
        });
    });

});

describe(".<http verb> works as expected", function () {
    it(".delete should work", function (done) {
        var app = express();
        app.delete('/', function (req, res) {
            res.send(200);
        });

        request(app)
            .delete('/')
            .expect(200, done);
    });
    
    it(".get should work", function (done) {
        var app = express();
        app.get('/', function (req, res) {
            res.send(200);
        });

        request(app)
            .get('/')
            .expect(200, done);
    });
    it(".post should work", function (done) {
        var app = express();
        app.post('/', function (req, res) {
            res.send(200);
        });

        request(app)
            .post('/')
            .expect(200, done);
    });
    it(".put should work", function (done) {
        var app = express();
        app.put('/', function (req, res) {
            res.send(200);
        });

        request(app)
            .put('/')
            .expect(200, done);
    });
});
