var  assert = require('assert'),
expect = require('chai').expect, 
should = require('chai').should();
const {
    isCorrectParams,
    isValidDate, isValidFormatPut, isValidFormatPost
} = require('../src/functions/functions')

/**
 * Testing functiones validadoras
 */
describe("Testing functiones validadoras", () => {
    it("should return true if valid Formartdata 2020/10/01", function () {
      assert.equal(isValidDate("2020/10/01"), true);
      //expect(isValid).to.be.true;
    });
    it("should return false if invalid Formatdata 2020-10-01", function () {
      var isInValid = isValidDate('2020-10-01')
      //assert.equal(isValid, false);
      isInValid.should.equal(false);
    });
  // validar las keys del body 
    it("should return true if equals vectors  ['client_id', 'object_id', 'details'] ==  ['client_id', 'object_id', 'details']", () => {
        var loQueRecibo = ['client_id', 'object_id', 'details'],
            comoDeberiaSer = ['object_id', 'client_id', 'details']
        expect(isCorrectParams(loQueRecibo, comoDeberiaSer)).to.be.true;
    });
    it("should return false if diferent vectors   [ 'object_id', 'details'] != ['client_id', 'object_id', 'details']", () => {
        var loQueRecibo = [ 'object_id', 'details'],
            comoDeberiaSer = ['object_id', 'client_id', 'details']
        isCorrectParams(loQueRecibo, comoDeberiaSer).should.equal(false);
    });
    it("should correct post", function(){
        var loQueRecibo =  {
            rent:{
                    client_id: [ 'b172a2ab-5900-4532-bd68-68a041752017' ],
                    object_id: [ '5d65ac9e-d431-4138-a8e4-c4719205cb1b' ],
                    details: [ {status:["RENTED"], until:["2020/10/01"]} ]
                }
        }
        assert.equal(isValidFormatPost(loQueRecibo), true);
    });
    it("should incorrect post", function(){
        var loQueRecibo =  [{
            "rent":[
                {
                    "object_id":["b172a2ab-5900-4532-bd68-68a041752017"],
                    "details":[{"status":"RENTED","until":"2020/10/01"}]
                }
            ]
        }], isInValid=isValidFormatPost(loQueRecibo)
        isInValid.should.equal(false);
    });
})